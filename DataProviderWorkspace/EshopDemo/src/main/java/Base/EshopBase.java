package Base;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EshopBase {
	
	private String url;
	private String value;
	private String title;
	public WebDriver driver;
	public WebDriverWait wait;
	public Select select;
	public EshopBase()
	{
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 10);
	}
	public void loadPropertiesFile() throws IOException
	{

	FileReader reader=new FileReader("Config.properties");  
    
    Properties p=new Properties();  
    p.load(reader);  
      
     url= p.getProperty("url");  
     value= p.getProperty("valueForSorting"); 
     title= p.getProperty("pageTitle");
	}

	public String getUrl() {
		return url;
	}

	public String getValue() {
		return value;
	}

	public String getTitle() {
		return title;
	}

	
}
