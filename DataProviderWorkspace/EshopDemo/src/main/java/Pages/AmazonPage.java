package Pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AmazonPage{

	 WebDriver driver;
	 
	 
	 @FindBy(xpath="//a[contains(text(),'Mobiles')]")
	 public WebElement mobileTab;
	 
	 @FindBy(xpath="//*[@id='gwd-browseMultiCategoryCard-merchandised-search-7']/div/div[1]/a/img")
	 public WebElement mobileCategory;
	 
	 @FindBy(xpath="//select[@id='sort']")
	 public WebElement sortDropdown;
	 
	 @FindBy(xpath="//span[@class='a-size-medium a-color-base a-text-normal']")
	 public List<WebElement> resultAfterSorting;
	 
	 @FindBy(xpath="//input[@id='add-to-cart-button']")
	 public WebElement addToCartButton;
	 
	 @FindBy(xpath="//a[@id = 'nav-cart']")
	 public WebElement cartLink;
	 
	 @FindBy(id="nav-cart-count")
	 public WebElement cartCount;
	 
	 @FindBy(xpath="//h1[contains(text(),'Added to Cart')]")
	 public WebElement addedToCartText;

	public AmazonPage(WebDriver driver){

        this.driver = driver;

        PageFactory.initElements(driver, this);

    } 
}


