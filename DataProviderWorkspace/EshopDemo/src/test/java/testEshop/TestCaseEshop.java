package testEshop;

import Pages.*;
import Base.*;
import Listener.*;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;




@Listeners(Listener.ListenerDemo.class)
public class TestCaseEshop extends EshopBase{
	
	AmazonPage amazonPage=new AmazonPage(driver);
	
	@BeforeMethod
	public void testSetup() throws IOException {
		loadPropertiesFile();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get(getUrl());
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			
	}
	
	@Test
	public void testEshop()
	{
		
		amazonPage.mobileTab.click();		
		amazonPage.mobileCategory.click();		
		select=new Select(amazonPage.sortDropdown);
		select.selectByValue(getValue());
		String parentWindow = driver.getWindowHandle();
		int i=0;				
     	for( i= 0; i<=1;i++)
		{
     		amazonPage.resultAfterSorting.get(i).click();
     		Set<String> handles=driver.getWindowHandles();
			for(String handle: handles)
			{
				driver.switchTo().window(handle);
				if(!driver.getTitle().equals(getTitle()))
				{
					amazonPage.addToCartButton.click();
					wait.until(ExpectedConditions.visibilityOf(amazonPage.addedToCartText));
					driver.close();
				}
			}
			driver.switchTo().window(parentWindow);	
		}
     	amazonPage.cartLink.click();
     	Assert.assertEquals(amazonPage.cartCount.getText(), String.valueOf(i));
	}
	
	@AfterMethod
	public void teardown()
	{
		driver.quit();
	}
	
	
}



