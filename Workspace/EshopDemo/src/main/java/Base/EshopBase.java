package Base;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class EshopBase {
	
	private String url;
	private String value;
	private String title;
	public WebDriver driver;
	public WebDriverWait wait;
	public Select select;
	public ExtentTest logger;
	public ExtentReports report;
	private int elementsToBeAddedToCart;
	public EshopBase()
	{
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 10);
		report = new ExtentReports(System.getProperty("user.dir")+"\\ExtentReportResults.html");
		
		
	}
	public void loadPropertiesFile() throws IOException
	{

	FileReader reader=new FileReader("Config.properties");  
    
    Properties p=new Properties();  
    p.load(reader);  
      
     url= p.getProperty("url");  
     value= p.getProperty("valueForSorting"); 
     title= p.getProperty("pageTitle");
     elementsToBeAddedToCart=Integer.parseInt(p.getProperty("elementsToBeAddedToCart"));
     
	}

	public String getUrl() {
		return url;
	}

	public String getValue() {
		return value;
	}

	public String getTitle() {
		return title;
	}
	
	public int getelementsToBeAddedToCart() {
		return elementsToBeAddedToCart;
	}
	
		
}
