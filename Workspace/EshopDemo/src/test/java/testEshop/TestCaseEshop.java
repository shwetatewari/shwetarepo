package testEshop;

import Pages.*;
import Base.*;
import Listener.*;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;




@Listeners(Listener.ListenerDemo.class)
public class TestCaseEshop extends EshopBase{
	
	AmazonPage amazonPage=new AmazonPage(driver);
	
	@BeforeMethod
	public void testSetup() throws IOException {
		loadPropertiesFile();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get(getUrl());
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
	}
	
	@Test
	public void testEshop()
	{
		logger = report.startTest("testEshop");
		amazonPage.mobileTab.click();
		logger.log(LogStatus.PASS, "Clicked on Mobile tab");
		amazonPage.mobileCategory.click();
		logger.log(LogStatus.PASS, "Clicked on Mobile category");
		select=new Select(amazonPage.sortDropdown);
		select.selectByValue(getValue());
		logger.log(LogStatus.PASS, "Sorted: price high to low");
		String parentWindow = driver.getWindowHandle();
		int i=0;				
     	for( i= 0; i<getelementsToBeAddedToCart();i++)
		{
     		amazonPage.resultAfterSorting.get(i).click();
     		logger.log(LogStatus.PASS, "Clicked on mobile "+ (i+1));
     		Set<String> handles=driver.getWindowHandles();
			for(String handle: handles)
			{
				driver.switchTo().window(handle);
				if(!driver.getTitle().equals(getTitle()))
				{
					amazonPage.addToCartButton.click();
					wait.until(ExpectedConditions.visibilityOf(amazonPage.addedToCartText));
					logger.log(LogStatus.PASS, "Number of mobiles added successfully to cart: "+ (i+1));
					driver.close();
				}
			}
			driver.switchTo().window(parentWindow);	
		}
     	amazonPage.cartLink.click();
     	Assert.assertEquals(amazonPage.cartCount.getText(), String.valueOf(i));
     	logger.log(LogStatus.PASS, "Test Case passed");
	}
	
	@AfterMethod
	public void teardown()
	{
		report.flush();
		report.endTest(logger);
		driver.quit();
	}
	
	
}



